import { Injectable } from '@nestjs/common';
//import { CreateCategoryDto } from './dto/create-category.dto';
//import { UpdateCategoryDto } from './dto/update-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CategoriesService {
  constructor(@InjectRepository(Category) private rep: Repository<Category>){

  }

  async crearCategoria(category: Category) {
    await this.rep.insert(category);
  }

  async obtenerCategorias(category: Category): Promise<Category[]> {
    return await this.rep.find();
  }

  async obtenerCategoria(_id: number): Promise<Category[]> {
    return await this.rep.find({
      select: ["id", "description", "color", "image"],
      where: [{"id": _id}]
    });
  }

  async actualizarCategoria(category: Category) {
    await this.rep.update({id: category.id}, category)
  }

  async borrarCategoria(category: Category) {
    await this.rep.delete(category)
  }
}
