import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { Category } from './entities/category.entity';
//import { CreateCategoryDto } from './dto/create-category.dto';
//import { UpdateCategoryDto } from './dto/update-category.dto';

@Controller('categories')
export class CategoriesController {
  constructor(private servicio: CategoriesService) {}

  @Post()
  create(@Body() category: Category) {
    return this.servicio.crearCategoria(category);
  }

  @Get()
  getAll(@Param() params) {
    return this.servicio.obtenerCategorias(params.id);
  }

  @Get(':id')
  get(@Param() params) {
    return this.servicio.obtenerCategorias(params.id);
  }

  @Put()
  update(@Body() category: Category){
    return this.servicio.actualizarCategoria(category);
  }

  @Delete(':id')
  delete(@Param() params){
      return this.servicio.borrarCategoria(params.id);
  }
    
}
