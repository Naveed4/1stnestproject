import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('category')
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 100})
    description: String;

    @Column({length: 30})
    color: String;

    @Column({length: 1000})
    image: String;
}
