import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('usuarios')
export class Usuario {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 50})
    nombre: String;

    @Column({length: 50})
    apellido: String;

    @Column()
    edad: number;

    @Column({length: 300})
    avatar: string;
}
